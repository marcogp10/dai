package es.uvigo.esei.dai.xml.sax;

import java.io.File;
import java.io.IOException;

import javax.xml.XMLConstants;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.ContentHandler;
import org.xml.sax.SAXException;
import org.xml.sax.XMLReader;

import es.uvigo.esei.dai.xml.SimpleErrorHandler;

public class SAXParsing {
	public static void parseFile(String xmlPath, ContentHandler handler)
	throws SAXException, IOException, ParserConfigurationException {
		// Construcción del parser SAX
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		parserFactory.setNamespaceAware(true);
		
		// Se añade el handler al parser SAX
		SAXParser parser = parserFactory.newSAXParser();
		XMLReader reader = parser.getXMLReader();
		reader.setContentHandler(handler);
		
		// Parsing
		reader.parse(xmlPath);
	}
	
	public static void parseAndValidatedWithInternalDTD(String xmlPath, ContentHandler handler)
	throws ParserConfigurationException, SAXException, IOException {
		// Construcción del parser SAX activando la validación
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		parserFactory.setValidating(true);
		
		// Al construir el parser hay que añadir un manejador de errores
		SAXParser parser = parserFactory.newSAXParser();
		XMLReader xmlReader = parser.getXMLReader();
		xmlReader.setContentHandler(handler);
		xmlReader.setErrorHandler(new SimpleErrorHandler());

		// Parsing
		xmlReader.parse(xmlPath);
	}
	
	public static void parseAndValidateWithInternalXSD(
		String xmlPath, ContentHandler handler
	) throws ParserConfigurationException, SAXException, IOException {
		// Construcción del parser del documento. Se activa
		// la validación y comprobación de namespaces
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		parserFactory.setValidating(true);
		parserFactory.setNamespaceAware(true);

		// Se añade el manejador de errores y se activa la validación por schema
		SAXParser parser = parserFactory.newSAXParser();
		parser.setProperty(
			"http://java.sun.com/xml/jaxp/properties/schemaLanguage",
			XMLConstants.W3C_XML_SCHEMA_NS_URI
		);
		XMLReader xmlReader = parser.getXMLReader();
		xmlReader.setContentHandler(handler);
		xmlReader.setErrorHandler(new SimpleErrorHandler());

		// Parsing
		xmlReader.parse(xmlPath);
	}
	
	public static void parseAndValidateWithExternalXSD(
		String xmlPath, String schemaPath, ContentHandler handler
	) throws ParserConfigurationException, SAXException, IOException {
		// Construcción del schema
		SchemaFactory schemaFactory = 
			SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
		Schema schema = schemaFactory.newSchema(new File(schemaPath));
		
		// Construcción del parser del documento. Se establece el esquema y se activa
		// la validación y comprobación de namespaces
		SAXParserFactory parserFactory = SAXParserFactory.newInstance();
		parserFactory.setValidating(false);
		parserFactory.setNamespaceAware(true);
		parserFactory.setSchema(schema);

		// Se añade el manejador de errores
		SAXParser parser = parserFactory.newSAXParser();
		XMLReader xmlReader = parser.getXMLReader();
		xmlReader.setContentHandler(handler);
		xmlReader.setErrorHandler(new SimpleErrorHandler());

		// Parsing
		xmlReader.parse(xmlPath);
	}
}
