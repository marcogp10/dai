package es.uvigo.esei.dai.xml.xpath;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.xml.namespace.NamespaceContext;
import javax.xml.namespace.QName;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.xml.sax.InputSource;

public class XPathUtils {
	public static Object xpathQuerySAX(
		InputSource xmlSource, String expression, QName returnType
	) throws XPathExpressionException {
		return xpathQuerySAX(xmlSource, expression, returnType, null);
	}
	
	public static Object xpathQueryDOM(
		Object object, String expression, QName returnType
	) throws XPathExpressionException {
		return xpathQueryDOM(object, expression, returnType, null);
	}
		
	public static Object xpathQuerySAX(
		InputSource xmlSource, String expression, QName returnType, NamespaceContext nsContext
	) throws XPathExpressionException {
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		
		if (nsContext != null)
			xpath.setNamespaceContext(nsContext);
		
		return xpath.evaluate(expression, xmlSource, returnType);
	}
	
	public static Object xpathQueryDOM(
		Object object, String expression, QName returnType, NamespaceContext nsContext
	) throws XPathExpressionException {
		XPathFactory factory = XPathFactory.newInstance();
		XPath xpath = factory.newXPath();
		
		if (nsContext != null)
			xpath.setNamespaceContext(nsContext);
		
		return xpath.evaluate(expression, object, returnType);
	}
	
	public final static class NamespaceContextMap implements NamespaceContext {
		private final Map<String, String> namespaceToPrefix;
		private final Map<String, String> prefixToNamespace;
		
		public NamespaceContextMap() {
			this.namespaceToPrefix = new HashMap<String, String>();
			this.prefixToNamespace = new HashMap<String, String>();
		}
		
		public void putNamespaceURI(String prefix, String namespaceURI) {
			this.namespaceToPrefix.put(namespaceURI, prefix);
			this.prefixToNamespace.put(prefix, namespaceURI);
		}
		
		@Override
		public String getNamespaceURI(String prefix) {
			return this.prefixToNamespace.get(prefix);
		}

		@Override
		public String getPrefix(String namespaceURI) {
			return this.namespaceToPrefix.get(namespaceURI);
		}

		@Override
		public Iterator<?> getPrefixes(String namespaceURI) {
			return this.namespaceToPrefix.keySet().iterator();
		}
	}
}
