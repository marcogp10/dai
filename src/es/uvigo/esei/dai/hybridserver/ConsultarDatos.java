package es.uvigo.esei.dai.hybridserver;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import javax.xml.namespace.QName;
import javax.xml.ws.Service;

public class ConsultarDatos {

	public static String consultar(Configuration config, String pagina, String tipo) throws MalformedURLException {

		try {
			List<ServerConfiguration> servidores = new ArrayList<ServerConfiguration>();
			servidores = config.getServers();
			for (ServerConfiguration sc : servidores) {
				URL url = new URL(sc.getWsdl());

				QName name = new QName(sc.getNamespace(), "PublicacionDeDatosService");
				Service service = Service.create(url, name);

				PublicarDatos pd = service.getPort(PublicarDatos.class);

				if (tipo.equals("html")) {
					List<String> lista = pd.getUuidHtml();
					if (lista.contains(pagina)) {
						return pd.getContentHtml(pagina);
					}
				} else if (tipo.equals("xml")) {
					List<String> lista = pd.getUuidXml();
					if (lista.contains(pagina)) {
						return pd.getContentXml(pagina);
					}
				} else if (tipo.equals("xsd")) {
					List<String> lista = pd.getUuidXsd();
					if (lista.contains(pagina)) {
						return pd.getContentXsd(pagina);
					}
				} else if (tipo.equals("xslt")) {
					List<String> lista = pd.getUuidXslt();
					if (lista.contains(pagina)) {
						return pd.getContentXslt(pagina);
					}
				} else if (tipo.equals("xsdDexslt")) {
					List<String> lista = pd.getUuidXslt();
					if (lista.contains(pagina)) {
						return pd.getXsd(pagina);
					}
				}

			}
			String toret = "404";
			return toret;
		} catch (Exception e) {
			String toret = "404";
			return toret;
		}
	}

	public static String listar(Configuration config, String tipo, String pagina) throws MalformedURLException {
		try {
			List<ServerConfiguration> servidores = new ArrayList<ServerConfiguration>();
			servidores = config.getServers();

			for (ServerConfiguration sc : servidores) {
				try {
					pagina += "<h1>" + sc.getName() + "</h1>";
					URL url = new URL(sc.getWsdl());
					QName name = new QName(sc.getNamespace(), "PublicacionDeDatosService");
					Service service = Service.create(url, name);

					PublicarDatos pd = service.getPort(PublicarDatos.class);

					if (tipo.equals("html")) {
						List<String> lista = pd.getUuidHtml();
						for (String i : lista) {
							pagina += " <div><li><a href=\"" + sc.getHttpAddress() + "html?uuid=" + i + "\">" + i
									+ "</a></li></div>";
						}
					} else if (tipo.equals("xml")) {
						List<String> lista = pd.getUuidXml();
						for (String i : lista) {
							pagina += " <div><li><a href=\"" + sc.getHttpAddress() + "xml?uuid=" + i + "\">" + i
									+ "</a></li></div>";
						}
					} else if (tipo.equals("xsd")) {
						List<String> lista = pd.getUuidXsd();
						for (String i : lista) {
							pagina += " <div><li><a href=\"" + sc.getHttpAddress() + "xsd?uuid=" + i + "\">" + i
									+ "</a></li></div>";
						}
					} else if (tipo.equals("xslt")) {
						List<String> lista = pd.getUuidXslt();
						for (String i : lista) {
							pagina += " <div><li><a href=\"" + sc.getHttpAddress() + "xslt?uuid=" + i + "\">" + i
									+ "</a></li></div>";
						}
					}
				} catch (Exception e) {
					e.printStackTrace();

				}

			}
			pagina += "</body></html>";
			return pagina;
		} catch (Exception e) {
			pagina += "</body></html>";
			return pagina;

		}
	}

}
