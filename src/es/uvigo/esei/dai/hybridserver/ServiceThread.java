package es.uvigo.esei.dai.hybridserver;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.Socket;

import es.uvigo.esei.dai.hybridserver.http.HTTPRequest;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequestMethod;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponse;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponseStatus;

//Marco García Pérez - Alejandro Molina Aranega

public class ServiceThread implements Runnable {
	 private final Socket socket;
	 private Paginas pagina;
	 
	 public ServiceThread(Socket clientSocket, Paginas pagina) throws IOException {
		 this.socket = clientSocket;
		 this.pagina = pagina;
	 }
		 @Override
	 
		 public void run() {
			 HTTPResponse response = new HTTPResponse();
		 try (Socket socket = this.socket) {
			 Reader reader = new InputStreamReader(socket.getInputStream());
				HTTPRequest request = new HTTPRequest(reader);
				
				
				String id;
				String contenido;
				
				response.setStatus(HTTPResponseStatus.S200);
				response.setVersion(request.getHttpVersion());
				
				
				if(request.getResourcePath().length<1){
					response.setContent("<html><body><h1>Hybrid Server</h1></body></html>");	
				
				}
				else if( (!request.getResourcePath()[0].equals("html"))){
					response.setStatus(HTTPResponseStatus.S400);
					response.setContent("<html><body><h1>Has hecho mal el get (400)</h1></body></html>");
					
					
				}
				else{
					if(request.getMethod().equals(HTTPRequestMethod.GET)){
						id = request.getResourceParameters().get("uuid");
						if(id != null){
							contenido = pagina.getPagina(id);
							if(contenido !=  null){
								response.setContent(contenido);
							}
							else{
								response.setStatus(HTTPResponseStatus.S404);
								response.setContent("<html><body><h1>Has hecho mal el get (404)</h1></body></html>");
								
							}
							
						}
						else{
							response.setContent(pagina.noUUID());
						}
					}
					
					
					else if(request.getMethod().equals(HTTPRequestMethod.POST)){
						
						if((contenido= request.getResourceParameters().get("html"))!=null &&
							request.getHeaderParameters().containsKey("Content-Length")){
							String nuevo = pagina.generarUUID();
							pagina.setPagina(nuevo, request.getContent());
							response.setContent("<a href=\"html?uuid=" + nuevo + "\">" + nuevo + "</a>");
							
							
						}
							else {
								response.setStatus(HTTPResponseStatus.S400);
								response.setContent("<html><body><h1>Post erroneo</h1></body></html>");
								
							}
					
					}
					
					
					else if(request.getMethod().equals(HTTPRequestMethod.DELETE)){
						id = request.getResourceParameters().get("uuid");
						if(id != null){
							contenido = pagina.getPagina(id);
							if(contenido !=  null){
								pagina.deletePagina(id);
								response.setContent("<html><body><h1>Eliminado</h1></body></html>");
							}
							else{
								response.setStatus(HTTPResponseStatus.S404);
								response.setContent("<html><body><h1>Quieres borrar una pagina sin contenido</h1></body></html>");
								
							}
							
						}
						else{
							response.setStatus(HTTPResponseStatus.S404);
							response.setContent("<html><body><h1>Quieres borrar una pagina </h1></body></html>");
							
						}
					}
					
					
					
					
				}
				System.out.println(response.toString());
				this.socket.getOutputStream().write(response.toString().getBytes());
		 }catch (Exception e){
			 response.setStatus(HTTPResponseStatus.S500);
				response.setContent("<html><body><h1>Error de servidor</h1></body></html>");
				
		// } catch (IOException | HTTPParseException e) {
			 e.printStackTrace();
		 }
	 }
	}