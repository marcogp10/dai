/**
 *  HybridServer
 *  Copyright (C) 2014 Miguel Reboiro-Jato
 *  
 *  This program is free software: you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation, either version 3 of the License, or
 *  (at your option) any later version.
 *  
 *  This program is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *  
 *  You should have received a copy of the GNU General Public License
 *  along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package es.uvigo.esei.dai.hybridserver;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import es.uvigo.esei.dai.xml.SimpleErrorHandler;

public class XMLConfigurationLoader {
	public Configuration load(File xmlFile)
	throws Exception {
		
		Configuration config = new Configuration();

			
			// Construcción del schema
			SchemaFactory schemaFactory = 
				SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);
			Schema schema = schemaFactory.newSchema(new File("configuration.xsd"));
			
			// Construcción del parser del documento. Se establece el esquema y se activa
			// la validación y comprobación de namespaces
			DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
			factory.setValidating(false);
			factory.setNamespaceAware(true);
			factory.setSchema(schema);
			
			// Se añade el manejador de errores
			DocumentBuilder builder = factory.newDocumentBuilder();
			builder.setErrorHandler(new SimpleErrorHandler());
			
			Document document = builder.parse(xmlFile);
			
			
			
			List<ServerConfiguration> servers =new ArrayList<ServerConfiguration>();
			
			
			config.setHttpPort(Integer.parseInt(document.getElementsByTagName("http").item(0).getTextContent()));
			config.setWebServiceURL(document.getElementsByTagName("webservice").item(0).getTextContent());
			config.setNumClients(Integer.parseInt(document.getElementsByTagName("numClients").item(0).getTextContent()));
			
			config.setDbUser(document.getElementsByTagName("user").item(0).getTextContent());
			config.setDbPassword(document.getElementsByTagName("password").item(0).getTextContent());
			config.setDbURL(document.getElementsByTagName("url").item(0).getTextContent());
			
			NodeList lista = document.getElementsByTagName("server");
			for(int i=0;i<lista.getLength();i++){
				Element element = (Element)lista.item(i);
				ServerConfiguration serverConfig = new ServerConfiguration(element.getAttribute("name"),element.getAttribute("wsdl"),
						element.getAttribute("namespace"),element.getAttribute("service"),element.getAttribute("httpAddress"));
				servers.add(serverConfig);
			}
			config.setServers(servers);

			return config;

	}
}
