package es.uvigo.esei.dai.hybridserver;

import java.io.File;

//Marco García Pérez - Alejandro Molina Aranega

public class Launcher {
	public static void main(String[] args) throws Exception {

		if (args.length > 0) {
			XMLConfigurationLoader xmlConfiguration;
			xmlConfiguration = new XMLConfigurationLoader();
			final Configuration configuration = xmlConfiguration.load(new File("test/configuration1.xml"));
			HybridServer server = new HybridServer(configuration);
			server.start();

			final Configuration configuration2 = xmlConfiguration.load(new File("test/configuration2.xml"));
			HybridServer server2 = new HybridServer(configuration2);
			server2.start();

			final Configuration configuration3 = xmlConfiguration.load(new File("test/configuration3.xml"));
			HybridServer server3 = new HybridServer(configuration3);
			server3.start();

			final Configuration configuration4 = xmlConfiguration.load(new File("test/configuration4.xml"));
			HybridServer server4 = new HybridServer(configuration4);
			server4.start();
		}

	}

}
