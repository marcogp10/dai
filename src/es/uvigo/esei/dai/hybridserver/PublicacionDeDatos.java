package es.uvigo.esei.dai.hybridserver;

import java.util.List;

import javax.jws.WebService;

@WebService(endpointInterface =
"es.uvigo.esei.dai.hybridserver.PublicarDatos")
public class PublicacionDeDatos implements PublicarDatos {

	/*
	private PaginasBD base;
	private XSD_DAO baseXSD;
	private XML_DAO baseXML;
	private XSLT_DAO baseXSLT;
	*/
	private GestorPaginasBD base;
	
	public PublicacionDeDatos(GestorPaginasBD base) {
		this.base = base;
	}
	

	@Override
	public List<String> getUuidHtml() {
		return base.list("html");
	}

	@Override
	public List<String> getUuidXml() {

		return base.list("xml");
	}

	@Override
	public List<String> getUuidXsd() {
		return base.list("xsd");
	}

	@Override
	public List<String> getUuidXslt() {
		return base.list("xslt");
	}

	@Override
	public String getContentHtml(String uuid) {
		return base.getPagina("html",uuid);
	}

	@Override
	public String getContentXml(String uuid) {
		return base.getPagina("xml",uuid);
	}

	@Override
	public String getContentXsd(String uuid) {
		return base.getPagina("xsd",uuid);
	}

	@Override
	public String getContentXslt(String uuid) {
		return base.getPagina("xslt",uuid);
	}

	@Override
	public String getXsd(String xslt) {
		return base.getXSD("xslt",xslt);
	}

}
