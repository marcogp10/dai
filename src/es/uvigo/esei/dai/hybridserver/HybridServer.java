package es.uvigo.esei.dai.hybridserver;

import java.io.IOException;

import java.net.ServerSocket;
import java.net.Socket;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import javax.xml.ws.Endpoint;

// Marco García Pérez - Alejandro Molina Aranega

public class HybridServer {

	private int SERVICE_PORT;
	private Thread serverThread;
	private boolean stop;
	private Paginas gestorPaginas;
	private Properties propiedades;
	private int numHilos;
	private String WebServiceURL;
	private String url;
	private String user;
	private String password;
	private boolean hayBD = true;
	private GestorPaginasBD base;
	private Configuration config;
	private PublicacionDeDatos pd;
	private Endpoint ep;
	private boolean endpointPublished = false;

	public static final String WEB_PAGE = "<html><body><h1>Prueba</h1></body></html>";

	public HybridServer() {
		this.SERVICE_PORT = 8888;
		this.numHilos = 30;
		this.url = "jdbc:mysql://localhost:3306/hstestdb";
		this.user = "hsdb";
		this.password = "hsdbpass";

		try {
			Configuration config = new Configuration();
			config.setDbPassword(password);
			config.setDbURL(url);
			config.setDbUser(user);
			this.configuraciones(config);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Acceso denegado a la BD");
			// e.printStackTrace();
		}

	}

	public HybridServer(Map<String, String> pages) {
		// Constructor necesario para los tests de la segunda semana
		SERVICE_PORT = 8888;
		hayBD = false;
		numHilos = 30;
		this.gestorPaginas = new Paginas(pages);
		gestorPaginas.setPuerto(SERVICE_PORT);
	}

	public HybridServer(Properties properties) {
		// Constructor necesario para los tests de la tercera semana
		this.propiedades = properties;
		this.SERVICE_PORT = Integer.valueOf(propiedades.getProperty("port"));
		this.numHilos = Integer.valueOf(propiedades.getProperty("numClients"));
		this.url = propiedades.getProperty("db.url");
		this.user = propiedades.getProperty("db.user");
		this.password = propiedades.getProperty("db.password");

		try {
			Configuration config = new Configuration();
			config.setDbPassword(password);
			config.setDbURL(url);
			config.setDbUser(user);
			this.configuraciones(config);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Acceso denegado a la BD");
			// e.printStackTrace();
		}

	}

	public HybridServer(Configuration config) {
		this.config = config;
		this.SERVICE_PORT = config.getHttpPort();
		this.numHilos = config.getNumClients();
		this.url = config.getDbURL();
		this.user = config.getDbUser();
		this.password = config.getDbPassword();
		this.WebServiceURL = config.getWebServiceURL();
		System.out.println(SERVICE_PORT + " -- " + numHilos + " -- " + url + " -- " + user + " -- " + password + " -- "
				+ WebServiceURL);
		try {
			this.configuraciones(config);

		} catch (Exception e) {
			// TODO Auto-generated catch block
			System.out.println("Acceso denegado a la BD");
			// e.printStackTrace();
		}
	}

	public void configuraciones(Configuration config) {
		base = new GestorPaginasBD(config);
		base.setPuerto(Integer.toString(SERVICE_PORT));
	}

	public int getPort() {
		return SERVICE_PORT;
	}

	public void start() {

		this.serverThread = new Thread() {
			@Override
			public void run() {
				try (final ServerSocket serverSocket = new ServerSocket(SERVICE_PORT)) {
					// Se crea el pool de hilos
					ExecutorService pool = Executors.newFixedThreadPool(numHilos);
					if (WebServiceURL != null) {
						pd = new PublicacionDeDatos(base);
						ep = Endpoint.publish(WebServiceURL, pd

						);
						endpointPublished = true;

					}
					while (true) {
						Socket socket = serverSocket.accept();
						if (stop)
							break;
						// Si hay BD como en la semana 3 se usa la BD si no la
						// hay, como en la semana 2 no se usa

						if (!hayBD) {
							pool.execute(new ServiceThread(socket, gestorPaginas));
						} else {

							pool.execute(new CompleteServiceThread(socket, base, config));

						}

					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		};

		this.stop = false;
		this.serverThread.start();
	}

	public void stop() {

		this.stop = true;

		try (Socket socket = new Socket("localhost", SERVICE_PORT)) {
			// Esta conexión se hace, simplemente, para "despertar" el hilo
			// servidor
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		if (endpointPublished) {
			try {
				ep.stop();
			} catch (Exception e) {
				e.printStackTrace();
				;
			}
		}
		try {
			this.serverThread.join();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}

		this.serverThread = null;
	}
}