package es.uvigo.esei.dai.hybridserver;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class GestorPaginasBD {
	private final Configuration config;
	private String puerto;

	public String getPuerto() {
		return puerto;
	}

	public void setPuerto(String puerto) {
		this.puerto = puerto;
	}

	public GestorPaginasBD(Configuration config) {
		this.config = config;
	}

	public void setPagina(String tipo, String uuid, String pagina, String xsd) {
		try (Connection connection = DriverManager.getConnection(this.config.getDbURL(), this.config.getDbUser(),
				this.config.getDbPassword())) {

			try (PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO " + tipo + " (uuid, xsd,content) VALUES (?, ?, ?)", Statement.RETURN_GENERATED_KEYS)) {
				statement.setString(1, uuid);
				statement.setString(3, pagina);
				statement.setString(2, xsd);

				if (statement.executeUpdate() != 1)
					throw new SQLException("Error insertando pagina");
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void createPagina(String tipo, String pagina) {
		try (Connection connection = DriverManager.getConnection(this.config.getDbURL(), this.config.getDbUser(),
				this.config.getDbPassword())) {

			try (PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO " + tipo + " (uuid, content) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS)) {
				statement.setString(1, UUID.randomUUID().toString());
				statement.setString(2, pagina);

				if (statement.executeUpdate() != 1)
					throw new SQLException("Error insertando pagina");
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void updatePagina(String tipo, String uuid, String pagina) {
		try (Connection connection = DriverManager.getConnection(this.config.getDbURL(), this.config.getDbUser(),
				this.config.getDbPassword())) {
			try (PreparedStatement statement = connection
					.prepareStatement("UPDATE " + tipo + " SET  content=?  WHERE uuid=?")) {
				statement.setString(1, pagina);
				statement.setString(2, uuid);
				if (statement.executeUpdate() != 1)
					throw new SQLException("Error actualizando pagina");
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public void setPagina(String tipo, String uuid, String pagina) {
		try (Connection connection = DriverManager.getConnection(this.config.getDbURL(), this.config.getDbUser(),
				this.config.getDbPassword())) {

			try (PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO " + tipo + " (uuid, content) VALUES (?, ?)", Statement.RETURN_GENERATED_KEYS)) {
				statement.setString(1, uuid);
				statement.setString(2, pagina);

				if (statement.executeUpdate() != 1)
					throw new SQLException("Error insertando pagina");
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void createPagina(String tipo, String pagina, String xsd) {
		try (Connection connection = DriverManager.getConnection(this.config.getDbURL(), this.config.getDbUser(),
				this.config.getDbPassword())) {

			try (PreparedStatement statement = connection.prepareStatement(
					"INSERT INTO " + tipo + " (uuid, content, xsd) VALUES (?, ?, ?)",
					Statement.RETURN_GENERATED_KEYS)) {
				statement.setString(1, UUID.randomUUID().toString());
				statement.setString(2, pagina);
				statement.setString(3, xsd);

				if (statement.executeUpdate() != 1)
					throw new SQLException("Error insertando pagina");
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void updatePagina(String tipo, String uuid, String pagina, String xsd) {
		try (Connection connection = DriverManager.getConnection(this.config.getDbURL(), this.config.getDbUser(),
				this.config.getDbPassword())) {
			try (PreparedStatement statement = connection
					.prepareStatement("UPDATE " + tipo + " SET  content=? xsd=? WHERE uuid=?")) {

				statement.setString(1, pagina);
				statement.setString(3, uuid);
				statement.setString(2, xsd);
				if (statement.executeUpdate() != 1)
					throw new SQLException("Error actualizando pagina");
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}

	public void deletePagina(String tipo, String uuid) {
		try (Connection connection = DriverManager.getConnection(this.config.getDbURL(), this.config.getDbUser(),
				this.config.getDbPassword())) {
			try (PreparedStatement statement = connection.prepareStatement("DELETE FROM " + tipo + " WHERE uuid=?")) {

				statement.setString(1, uuid);

				if (statement.executeUpdate() != 1)

					throw new SQLException("Error eliminando pagina");
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public String getPagina(String tipo, String uuid) {
		try (Connection connection = DriverManager.getConnection(this.config.getDbURL(), this.config.getDbUser(),
				this.config.getDbPassword())) {
			try (PreparedStatement statement = connection
					.prepareStatement("SELECT content FROM " + tipo + " WHERE uuid=?")) {

				statement.setString(1, uuid);

				try (ResultSet result = statement.executeQuery()) {
					if (result.next()) {
						return result.getString("content");
					} else {
						String toret = "404";
						return toret;
					}
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public String getXSD(String tipo, String uuid) {
		if (!tipo.equals("xslt")) {
			String toret = "404";
			return toret;
		}
		try (Connection connection = DriverManager.getConnection(this.config.getDbURL(), this.config.getDbUser(),
				this.config.getDbPassword())) {
			try (PreparedStatement statement = connection.prepareStatement("SELECT xsd FROM XSLT WHERE uuid=?")) {

				statement.setString(1, uuid);

				try (ResultSet result = statement.executeQuery()) {
					if (result.next()) {
						return result.getString("xsd");
					} else {
						String toret = "404";
						return toret;
					}
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public List<String> list(String tipo) {
		final List<String> paginas;
		try (Connection connection = DriverManager.getConnection(this.config.getDbURL(), this.config.getDbUser(),
				this.config.getDbPassword())) {
			try (PreparedStatement statement = connection.prepareStatement("SELECT uuid FROM " + tipo)) {

				try (ResultSet result = statement.executeQuery()) {

					paginas = new ArrayList<>();

					while (result.next()) {
						paginas.add(result.getString("uuid"));
					}

				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return paginas;
	}

	public String noUUID(String tipo) {
		try (Connection connection = DriverManager.getConnection(this.config.getDbURL(), this.config.getDbUser(),
				this.config.getDbPassword())) {
			try (PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + tipo)) {

				try (ResultSet result = statement.executeQuery()) {
					final Map<String, String> paginas = new LinkedHashMap<>();

					while (result.next()) {
						paginas.put(result.getString("uuid"), result.getString("content"));

					}

					String pagina = "<html><body><h1>Hybrid Server</h1>";
					for (String i : paginas.keySet()) {
						pagina += " <div><li><a href=\"http://localhost:" + this.config.getDbURL() + "/" + tipo
								+ "?uuid=" + i + "\">" + i + "</a></li></div>";
					}
					pagina += "</body></html>";
					return pagina;
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public String generarUUID() {

		return (UUID.randomUUID().toString());
	}
}
