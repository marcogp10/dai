package es.uvigo.esei.dai.hybridserver;

import java.util.List;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public interface PublicarDatos {

	@WebMethod
	public List<String> getUuidHtml();

	@WebMethod
	public List<String> getUuidXml();

	@WebMethod
	public List<String> getUuidXsd();

	@WebMethod
	public List<String> getUuidXslt();

	@WebMethod
	public String getContentHtml(String uuid);

	@WebMethod
	public String getContentXml(String uuid);

	@WebMethod
	public String getContentXsd(String uuid);

	@WebMethod
	public String getContentXslt(String uuid);

	@WebMethod
	public String getXsd(String xslt);

}
