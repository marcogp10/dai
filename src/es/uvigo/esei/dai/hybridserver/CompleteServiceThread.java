package es.uvigo.esei.dai.hybridserver;

import java.io.IOException;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.StringReader;
import java.io.StringWriter;
import java.net.Socket;
import java.util.List;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;

import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;

import org.xml.sax.InputSource;

import es.uvigo.esei.dai.hybridserver.http.HTTPParseException;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequest;
import es.uvigo.esei.dai.hybridserver.http.HTTPRequestMethod;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponse;
import es.uvigo.esei.dai.hybridserver.http.HTTPResponseStatus;
import es.uvigo.esei.dai.xml.SimpleErrorHandler;

//Marco García Pérez - Alejandro Molina Aranega

public class CompleteServiceThread implements Runnable {
	private final Socket socket;
	private GestorPaginasBD base;
	private Configuration config;

	public CompleteServiceThread(Socket clientSocket, GestorPaginasBD pagina, Configuration config) throws IOException {
		this.socket = clientSocket;
		this.base = pagina;
		this.config = config;
	}

	@Override

	public void run() {

		HTTPResponse response = new HTTPResponse();

		try (Socket socket = this.socket) {
			try {
				Reader reader = new InputStreamReader(socket.getInputStream());
				HTTPRequest request = new HTTPRequest(reader);

				String id;
				String contenido;
				String tipo;

				response.setStatus(HTTPResponseStatus.S200);
				response.setVersion(request.getHttpVersion());

				if (request.getResourcePath().length < 1 && request.getMethod().equals(HTTPRequestMethod.GET)) {
					response.setContent("<html><body><h1>Hybrid Server</h1></body></html>");

				} else if (request.getResourcePath().length != 1) {
					response.setStatus(HTTPResponseStatus.S404);
					response.setContent(
							"<html><body><h1>Has hecho mal el get (Directorio invalido) (404)</h1></body></html>");

				} else {
					tipo = request.getResourcePath()[0];
					if (tipo.equals("html") || tipo.equals("xsd") || tipo.equals("xslt") || tipo.equals("xml")) {
						if (request.getMethod().equals(HTTPRequestMethod.GET)) {

							id = request.getResourceParameters().get("uuid");
							if (id != null) {
								contenido = base.getPagina(tipo, id);
								if (!contenido.equals("404")) {
									response.setContent(contenido);
									if (tipo.equals("xml")) {
										response = this.transformarXML(response, request);
									} else if (tipo.equals("html")) {
										response.putParameter("Content-Type", "text/html");
									} else {
										response.putParameter("Content-type", "application/xml");
									}
								} else {
									String resultado = ConsultarDatos.consultar(config, id, tipo);
									if (!resultado.equals("404")) {
										response.setContent(resultado);
										if (tipo.equals("xml")) {
											response = this.transformarXML(response, request);
										} else if (tipo.equals("html")) {
											response.putParameter("Content-Type", "text/html");
										} else {
											response.putParameter("Content-type", "application/xml");
										}
									} else {
										response.setStatus(HTTPResponseStatus.S404);
										response.setContent(
												"<html><body><h1>Has hecho mal el get (404)</h1></body></html>");
									}
								}

							} else {
								String pagina = "<html><body>";
								List<String> lista = base.list(tipo);
								for (String i : lista) {
									pagina += " <div><li><a href=\"http://localhost:" + base.getPuerto() + "/" + tipo
											+ "?uuid=" + i + "\">" + i + "</a></li></div>";
								}
								String resultado = ConsultarDatos.listar(config, tipo, pagina);
								response.setContent(resultado);
								if (tipo.equals("html")) {
									response.putParameter("Content-Type", "text/html");
								} else {
									response.putParameter("Content-type", "application/xml");
								}
							}
						}

						else if (request.getMethod().equals(HTTPRequestMethod.POST)) {

							if ((contenido = request.getResourceParameters().get(tipo)) != null
									&& request.getHeaderParameters().containsKey("Content-Length")
									&& ((request.getResourceParameters().get("xsd") != null) == (tipo.equals("xslt"))
											|| tipo.equals("xsd"))) {
								String nuevo = base.generarUUID();
								if (tipo.equals("xslt")) {

									if (!base.getPagina("xsd", request.getResourceParameters().get("xsd"))
											.equals("404")) {
										base.setPagina(tipo, nuevo, request.getContent(),
												request.getResourceParameters().get("xsd"));
										response.putParameter("Content-type", "application/xml");
										response.setStatus(HTTPResponseStatus.S200);
										response.setContent(
												"<a href=\"" + tipo + "?uuid=" + nuevo + "\">" + nuevo + "</a>");
									} else {
										String resultado = ConsultarDatos.consultar(config,
												request.getResourceParameters().get("xsd"), "xsd");
										if (resultado.equals("404")) {
											response.setStatus(HTTPResponseStatus.S404);
											response.setContent(
													"<html><body><h1>XSD erroneo erroneo</h1></body></html>");
										} else {
											base.setPagina(tipo, nuevo, request.getContent(),
													request.getResourceParameters().get("xsd"));
											response.putParameter("Content-type", "application/xml");
											response.setStatus(HTTPResponseStatus.S200);
											response.setContent(
													"<a href=\"" + tipo + "?uuid=" + nuevo + "\">" + nuevo + "</a>");
										}

									}

								} else {
									base.setPagina(tipo, nuevo, request.getContent());

									if (tipo.equals("html")) {
										response.putParameter("Content-Type", "text/html");
									} else {
										response.putParameter("Content-type", "application/xml");
									}
									response.setContent(
											"<a href=\"" + tipo + "?uuid=" + nuevo + "\">" + nuevo + "</a>");
									response.setStatus(HTTPResponseStatus.S200);
								}
							} else {
								response.setStatus(HTTPResponseStatus.S400);
								response.setContent("<html><body><h1>Post erroneo</h1></body></html>");

							}

						}

						else if (request.getMethod().equals(HTTPRequestMethod.DELETE)) {
							id = request.getResourceParameters().get("uuid");
							if (id != null) {
								contenido = base.getPagina(tipo, id);
								if (!contenido.equals("404")) {
									base.deletePagina(tipo, id);
									response.setContent("<html><body><h1>Eliminado</h1></body></html>");
								} else {
									response.setStatus(HTTPResponseStatus.S404);
									response.setContent(
											"<html><body><h1>Quieres borrar una pagina sin contenido</h1></body></html>");

								}

							} else {
								response.setStatus(HTTPResponseStatus.S404);
								response.setContent("<html><body><h1>Quieres borrar una pagina </h1></body></html>");

							}
						}
					} else {
						response.setStatus(HTTPResponseStatus.S404);
						response.setContent("<html><body><h1>Directorio invalido </h1></body></html>");

					}
				}
				System.out.println(response.toString());
				this.socket.getOutputStream().write(response.toString().getBytes());
			} catch (IOException | HTTPParseException e) {
				response.setStatus(HTTPResponseStatus.S500);
				response.setContent("<html><body><h1>Error de servidor</h1></body></html>");
				System.out.println(response.toString());
				try {
					this.socket.getOutputStream().write(response.toString().getBytes());
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}

			}
		} catch (Exception e) {

			e.printStackTrace();
		}
	}

	private HTTPResponse transformarXML(HTTPResponse response, HTTPRequest request) {
		String id;
		String contenido;
		id = request.getResourceParameters().get("uuid");
		try {
			if (id != null) {
				contenido = base.getPagina("xml", id);
				if (contenido.equals("404")) {
					contenido = ConsultarDatos.consultar(config, id, "xml");

				}
				if (contenido.equals("404")) {
					response.setStatus(HTTPResponseStatus.S404);
					response.setContent("<html><body><h1>Has hecho mal el get (404)</h1></body></html>");
				} else {
					if (request.getResourceParameters().containsKey("xslt")) {
						String xslt = base.getPagina("xslt", request.getResourceParameters().get("xslt"));
						String xsd = base.getXSD("xslt", request.getResourceParameters().get("xslt"));
						if (xslt.equals("404")) {
							xslt = ConsultarDatos.consultar(config, request.getResourceParameters().get("xslt"),
									"xslt");
							xsd = ConsultarDatos.consultar(config, request.getResourceParameters().get("xslt"),
									"xsdDexslt");
						}
						if (xslt.equals("404")) {
							response.setStatus(HTTPResponseStatus.S404);
							response.setContent("<html><body><h1>No hay xslt (404)</h1></body></html>");
						} else {
							String xsdcontent = base.getPagina("xsd", xsd);
							if (xsdcontent.equals("404")) {
								xsdcontent = ConsultarDatos.consultar(config, xsd, "xsd");
							}
							if (xsdcontent.equals("404")) {
								response.setStatus(HTTPResponseStatus.S404);
								response.setContent("<html><body><h1>No hay xsd (404)</h1></body></html>");
							} else {
								try {

									SchemaFactory sf = SchemaFactory.newInstance(XMLConstants.W3C_XML_SCHEMA_NS_URI);

									Source sc = new StreamSource(new StringReader(xsdcontent));
									Schema schema = sf.newSchema(sc);

									DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
									factory.setValidating(false);
									factory.setNamespaceAware(true);
									factory.setSchema(schema);

									DocumentBuilder db = factory.newDocumentBuilder();
									db.setErrorHandler(new SimpleErrorHandler());
									InputSource is = new InputSource();
									is.setCharacterStream(new StringReader(contenido));

									db.parse(is);

									StringWriter writer = new StringWriter();
									StreamResult result = new StreamResult(writer);

									TransformerFactory tFactory = TransformerFactory.newInstance();
									Transformer transformer = tFactory
											.newTransformer(new StreamSource(new StringReader(xslt)));
									transformer.transform(new StreamSource(new StringReader(contenido)), result);

									response.setContent(writer.toString());
									response.putParameter("Content-Type", "text/html");

								} catch (Exception e) {
									response.setStatus(HTTPResponseStatus.S400);
									response.setContent("<html><body><h1>No valida el xsd (400)</h1></body></html>");

									e.printStackTrace();

								}
							}
						}
					} else {
						response.setContent(contenido);
						response.putParameter("Content-type", "application/xml");
					}
				}
			}
		} catch (Exception e) {

		}
		return response;
	}

}
