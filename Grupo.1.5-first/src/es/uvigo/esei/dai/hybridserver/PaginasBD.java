package es.uvigo.esei.dai.hybridserver;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

//Marco García Pérez - Alejandro Molina Aranega

public class PaginasBD {
	private final Connection connection;
	private String puerto;
		
	public PaginasBD(Connection connection) {
		this.connection = connection;
	}
	
	public String getPuerto() {
		return puerto;
	}

	public void setPuerto(String puerto) {
		this.puerto = puerto;
	}
	
	public void setPagina(String uuid, String pagina){
		try (PreparedStatement statement = this.connection.prepareStatement(
				"INSERT INTO HTML (uuid, content) VALUES (?, ?)",
				Statement.RETURN_GENERATED_KEYS
			)) {
				statement.setString(1, uuid);
				statement.setString(2, pagina);
				
				if (statement.executeUpdate() != 1)
					throw new SQLException("Error insertando pagina");
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
		}
	
	public void createPagina(String pagina) {
		try (PreparedStatement statement = this.connection.prepareStatement(
			"INSERT INTO HTML (uuid, content) VALUES (?, ?)",
			Statement.RETURN_GENERATED_KEYS
		)) {
			statement.setString(1, UUID.randomUUID().toString());
			statement.setString(2, pagina);
			
			if (statement.executeUpdate() != 1)
				throw new SQLException("Error insertando pagina");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}

	public void updatePagina(String uuid, String pagina){
		try (PreparedStatement statement = this.connection.prepareStatement(
			"UPDATE HTML SET  content=? WHERE uuid=?"
		)) {
			statement.setString(1, pagina);
			statement.setString(2, uuid);
			if (statement.executeUpdate() != 1)
				throw new SQLException("Error actualizando pagina");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}

	}


	public void deletePagina(String uuid){
		try (PreparedStatement statement = this.connection.prepareStatement(
			"DELETE FROM HTML WHERE uuid=?"
		)) {
			statement.setString(1, uuid);
			
			if (statement.executeUpdate() != 1)
				
				throw new SQLException("Error eliminando pagina");
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}


	public String getPagina(String uuid) {
		try (PreparedStatement statement = this.connection.prepareStatement(
			"SELECT content FROM HTML WHERE uuid=?"
		)) {
			statement.setString(1, uuid);
			
			try (ResultSet result = statement.executeQuery()) {
				if (result.next()) { 
					return result.getString("content");
				} else {
					String toret = "404"; 
					return  toret;
				}
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}


	public List<String> list() {
		final List<String> paginas;
		try (Statement statement = this.connection.createStatement()) {
			try (ResultSet result = statement.executeQuery("SELECT content FROM HTML")) {
				 paginas= new ArrayList<>();
				
				while (result.next()) {
					paginas.add(result.getString("content"));
				}
				
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
		return paginas;
	}
	
	
	public String noUUID(){
		try (Statement statement = this.connection.createStatement()) {
			try (ResultSet result = statement.executeQuery("SELECT * FROM HTML")) {
				final Map<String,String> paginas= new LinkedHashMap<>();
				
				while (result.next()) {
					paginas.put(result.getString("uuid"), result.getString("content"));
					
				}
				
				String pagina =  "<html><body><h1>Hybrid Server</h1>";
				for(String i:paginas.keySet()){
					pagina += " <div><li><a href=\"http://localhost:"+ puerto+"/html?uuid="+i+"\">"+i+"</a></li></div>";
				}
				pagina += "</body></html>";
				return pagina;
			}
		} catch (SQLException e) {
			throw new RuntimeException(e);
		}
	}
	
	 public boolean exist(String uuid){
			final List<String> paginas;
			try (Statement statement = this.connection.createStatement()) {
				try (ResultSet result = statement.executeQuery("SELECT uuid FROM HTML")) {
					 paginas= new ArrayList<>();
					
					while (result.next()) {
						paginas.add(result.getString("content"));
					}
					
				}
			} catch (SQLException e) {
				throw new RuntimeException(e);
			}
			return paginas.contains(uuid);
	 }
	
	
	public String generarUUID(){
		
		return (UUID.randomUUID().toString());
	}
	
}
