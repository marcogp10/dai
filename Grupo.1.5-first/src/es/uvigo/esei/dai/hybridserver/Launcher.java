package es.uvigo.esei.dai.hybridserver;

import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;



//Marco García Pérez - Alejandro Molina Aranega

public class Launcher {
	public static void main(String[] args) throws Exception {
		
		File fConfiguracion = new File(args[0]);
		FileInputStream isConfig = new FileInputStream(fConfiguracion);
		Properties propiedades = new Properties();
		propiedades.load(isConfig);
		HybridServer server= new HybridServer(propiedades);
		server.start();		
	}



}
