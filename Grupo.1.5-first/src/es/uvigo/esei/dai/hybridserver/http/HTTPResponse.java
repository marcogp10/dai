package es.uvigo.esei.dai.hybridserver.http;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

//Marco García Pérez - Alejandro Molina Aranega

public class HTTPResponse {
	
	
	private HTTPResponseStatus status;
	private String version;
	private String contenido ="";
	private Map<String, String> headerParameters = new LinkedHashMap<String, String>();
	

	
	public HTTPResponse() {
		
	}

	public HTTPResponseStatus getStatus() {
		
		
		
		return status;
	}

	public void setStatus(HTTPResponseStatus status) {
		
		
		this.status=status;
		
	
	}

	public String getVersion() {
		
		return version;
	}

	public void setVersion(String version) {
		this.version=version;
	}

	public String getContent() {
		// TODO Auto-generated method stub
		return contenido;
	}

	public void setContent(String content) {
	headerParameters.put("Content-Length", Integer.toString(content.length()));
		this.contenido=content;
	
	}

	public Map<String, String> getParameters() {
		
		return headerParameters;
	}

	public String putParameter(String name, String value) {
		String aux = headerParameters.put(name, value);
		return aux;
	}

	public boolean containsParameter(String name) {
		
		return headerParameters.containsKey(name);
	}

	public String removeParameter(String name) {
		
		return headerParameters.remove(name);
	}

	public void clearParameters() {
		headerParameters.clear();
	}

	public List<String> listParameters() {
		
		List<String> aux = new ArrayList<String>(); 
		aux.addAll(headerParameters.keySet());
		return aux;
	}

	public void print(Writer writer) throws IOException {
	
			writer.write(version+" "+Integer.toString(status.getCode()) +" "+status.getStatus()+"\r\n");
			
			for (Map.Entry<String, String> aux : this.getParameters().entrySet()) {
				
				writer.write(aux.getKey()+": "+aux.getValue()+"\r\n");
			}

			
			   if(headerParameters.containsKey("Content-Length")){
			    	String valor = headerParameters.get("Content-Length");
			   
			    	int length = Integer.parseInt(valor); 
			    	if (length>0) {
						writer.write("\r\n"+this.getContent());
					}
			    }else{
			
			   writer.write("\r\n");
			    }
	}

	@Override
	public String toString() {
		final StringWriter writer = new StringWriter();

		try {
			this.print(writer);
		} catch (IOException e) {
		}

		return writer.toString();
	}
}
