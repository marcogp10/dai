package es.uvigo.esei.dai.hybridserver.http;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.Reader;
import java.util.LinkedHashMap;
import java.util.Map;

//Marco García Pérez - Alejandro Molina Aranega

public class HTTPRequest {
		
		private String metodo;
		private String resource;
		private String version;
		private String contenido = null;
		
		private Map<String, String> resourceParameters = new LinkedHashMap<String, String>();
		private Map<String, String> headerParameters = new LinkedHashMap<String, String>();
		
	
		
		
	public HTTPRequest(Reader reader) throws IOException, HTTPParseException {
		System.out.println();
		
		BufferedReader	br = new BufferedReader(reader);
		
		String line;
		try {
			
			
			
			String res[] = br.readLine().split(" ");
			if(res.length!=3){
				throw new HTTPParseException();
			}
			metodo = res[0];
						
			resource = res[1];
							
			
			version = res[2];
			
		
			
			System.out.println("Metodo: "+ metodo);
			System.out.println("Recurso: "+resource);
			System.out.println("Version: "+version);

			
			
			if(resource.contains("?")) {
				String params[] = resource.split("\\?");
				
				for(String pair: params[1].split("&")) {
					resourceParameters.put(pair.split("=")[0],pair.split("=")[1]);
				}
			}
			
		
			
			
			line = br.readLine();
		
			
			while(line != null && !line.equals("")) {
				String[] hparametros = line.split(": ");
				if(hparametros.length < 2){
					throw new HTTPParseException();
				}
				
				headerParameters.put(line.split(": ")[0],line.split(": ")[1]);
				line = br.readLine();
				System.out.println(line);
			}
			
		
			if((this.getMethod()==HTTPRequestMethod.POST) &&  (headerParameters.containsKey("Content-Length"))) {
				
				int longitud = Integer.valueOf(headerParameters.get("Content-Length"));
				char [] auxiliar = new char[longitud];
				br.read(auxiliar, 0, longitud);
				
				contenido =  String.valueOf(auxiliar);

				System.out.println("Contenido: "+ contenido);

					String result = java.net.URLDecoder.decode(contenido, "UTF-8");					
					for(String pair: result.split("&")) {
					
						resourceParameters.put(pair.split("=")[0],pair.split("=")[1]);
					}

				contenido = java.net.URLDecoder.decode(contenido, "UTF-8");
				
				if(contenido.contains("html="))
				contenido = contenido.split("=")[1];
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
			
   
	public HTTPRequestMethod getMethod()  {
		
		
        
		switch (metodo) {
        case "HEAD":
            return HTTPRequestMethod.HEAD;
                
        case "GET":
            return HTTPRequestMethod.GET;           
                     
        case "POST":
            return HTTPRequestMethod.POST;           
            
        case "PUT":
            return HTTPRequestMethod.PUT;          
            
        case "DELETE":
            return HTTPRequestMethod.DELETE;          
            
        case "TRACE":
            return HTTPRequestMethod.TRACE;          
            
        case "OPTIONS":
            return HTTPRequestMethod.OPTIONS;          
            
        case "CONNECT":
            return HTTPRequestMethod.CONNECT;
                                
        default:
            break;
    }
		
		return null;
	}

	
	public String getResourceChain() {
	
		return resource;
	}
	

	
	public String[] getResourcePath() {
		String[] ResourcePath;
		if(resource.equals("/")){
			ResourcePath= new String[0];
			return ResourcePath;
		}
		
		String[] aux;
		if(resource.contains("?")) {
			String name[] = resource.split("\\?");
			

			aux = name[0].split("/");	
		}
		
		else aux = resource.split("/");	
		
		
		ResourcePath= new String[aux.length-1];
		for(int i=1;i<aux.length;i++){
			ResourcePath[i-1]=aux[i];		
			}
		return ResourcePath;		
		
	}

	
	public String getResourceName() { 
		
		if(resource.equals("/")){
			return "";
		}

		String[] aux;
		if(resource.contains("?")) {
			String name[] = resource.split("\\?");
			

			aux = name[0].split("/");	
		}
		
		else aux = resource.split("/");	
		String resourcename = aux[1];
		
		for(int i =2;i<aux.length;i++){
			resourcename =resourcename +"/"+aux[i];
			
		}
		return resourcename;
		
		
		
	}


	
	public Map<String, String> getResourceParameters() {
		
		 return resourceParameters;
		}
	
	public String getHttpVersion() {
		return version;
	}


	public Map<String, String> getHeaderParameters() {
		
		return headerParameters;
	}

	
	public String getContent() {
	
			return contenido;
		}
		
	
	public int getContentLength() {
	    if(headerParameters.containsKey("Content-Length")){
	    	String valor = headerParameters.get("Content-Length");
	   
	    	int length = Integer.parseInt(valor); 
	    	return length;
	    }
	    return 0;
	}

	@Override
	public String toString() {
		final StringBuilder sb = new StringBuilder(this.getMethod().name()).append(' ').append(this.getResourceChain())
				.append(' ').append(this.getHttpVersion()).append("\r\n");

		for (Map.Entry<String, String> param : this.getHeaderParameters().entrySet()) {
			sb.append(param.getKey()).append(": ").append(param.getValue()).append("\r\n");
		}

		if (this.getContentLength() > 0) {
			sb.append("\r\n").append(this.getContent());
		}

		return sb.toString();
	}
}
