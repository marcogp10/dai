package es.uvigo.esei.dai.hybridserver;

import java.io.IOException;

import java.net.ServerSocket;
import java.net.Socket;
import java.sql.DriverManager;
import java.sql.SQLException;

import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;


// Marco García Pérez - Alejandro Molina Aranega


public class HybridServer {

	private int SERVICE_PORT;
	private Thread serverThread;
	private boolean stop;
	private Paginas gestorPaginas;
	private Properties propiedades;
	private int numHilos;
	
	private String url;
	private String user;
	private	String password;
	private boolean hayBD = true;
	private PaginasBD base;
	
	
	
	public static final String WEB_PAGE = "<html><body><h1>Prueba</h1></body></html>";
	
	
	public HybridServer() {
		SERVICE_PORT = 8888;
		numHilos = 30;
		hayBD=false;
	}

	public HybridServer(Map<String, String> pages) {
		// Constructor necesario para los tests de la segunda semana
		SERVICE_PORT = 8888;
		hayBD=false;
		numHilos = 30;
		this.gestorPaginas = new Paginas(pages);
		gestorPaginas.setPuerto(SERVICE_PORT);
	}

	public HybridServer(Properties properties) {
		// Constructor necesario para los tests de la tercera semana
		this.propiedades=properties;
		this.SERVICE_PORT = Integer.valueOf(propiedades.getProperty("port"));
		this.numHilos = Integer.valueOf(propiedades.getProperty("numClients"));
		this.url = propiedades.getProperty("db.url");
		this.user = propiedades.getProperty("db.user");
		this.password = propiedades.getProperty("db.password");
		
		try {
			base = new PaginasBD(DriverManager.getConnection(
			url, user, password
			));
					
			base.setPuerto(String.valueOf(SERVICE_PORT));
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println("Acceso denegado a la BD");
			//e.printStackTrace();
		}

		
	}

	public int getPort() {
		// TODO Auto-generated method stub
		return SERVICE_PORT;
	}

	public void start() {
		
		
		this.serverThread = new Thread() {
			@Override
			public void run() {
				try (final ServerSocket serverSocket = new ServerSocket(SERVICE_PORT)) {
					//Se crea el pool de hilos
					ExecutorService pool = Executors.newFixedThreadPool(numHilos);
					
					while (true) {
						Socket socket = serverSocket.accept();					
						if(stop) break;
						//Si hay BD como en la semana 3 se usa la BD si no la hay, como en la semana 2 no se usa
					
						if(!hayBD){
							pool.execute(new ServiceThread(socket,gestorPaginas));
						}else{
							pool.execute(new ServiceThreadBD(socket,base));
						}
						
						
					}
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		};

		this.stop = false;
		this.serverThread.start();
	}

	public void stop() {
		this.stop = true;
		
		try (Socket socket = new Socket("localhost", SERVICE_PORT)) {
			// Esta conexión se hace, simplemente, para "despertar" el hilo servidor
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
		
		try {
			this.serverThread.join();
		} catch (InterruptedException e) {
			throw new RuntimeException(e);
		}
		
		this.serverThread = null;
	}
}