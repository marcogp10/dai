<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
	xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
	xmlns:tns="http://www.esei.uvigo.es/dai/hybridserver"
	>
	<xsl:output method="html" indent="yes" encoding="utf-8"/>
	<xsl:template match="/">
			<html>
			<head>
				<title>Configuraciones</title>
			</head>
			<body>
				<div id="container">
					<h1>Conexion</h1>
					<xsl:apply-templates select="tns:configuration/tns:connections"/>
					<h1>Database</h1>
					<xsl:apply-templates select="tns:configuration/tns:database"/>
					<h1>Servers</h1>
					<xsl:for-each select="tns:configuration/tns:servers/tns:server">
						<li>
							<h2>name:&#160; <xsl:value-of select="@name"/></h2>
						</li>
						<li>
							<strong>wsdl:</strong>&#160; <xsl:value-of select="@wsdl"/>
						</li>
						<li>
							<strong>namespace:</strong>&#160; <xsl:value-of select="@namespace"/>
						</li>
						<li>
							<strong>service:</strong>&#160; <xsl:value-of select="@service"/>
						</li>
						<li>
							<strong>httpAddress:</strong>&#160; <xsl:value-of select="@httpAddress"/>
						</li>

					</xsl:for-each>


					
				</div>
			</body>
			</html>
	</xsl:template>


	<xsl:template match="tns:configuration/tns:connections">
		<div>
			<div class="http">
					<strong>http:</strong>&#160; <xsl:value-of select="tns:http"/>
			</div>
			<div class="webservice">
					<strong>webservice:</strong>&#160; <xsl:value-of select="tns:webservice"/>
			</div>	
			<div class="numClients">
					<strong>numClients:</strong>&#160; <xsl:value-of select="tns:numClients"/>
			</div>	
		</div>
	</xsl:template>	

	<xsl:template match="tns:configuration/tns:database">
		<div>
			<div class="user">
					<strong>user:</strong>&#160; <xsl:value-of select="tns:user"/>
			</div>
			<div class="password">
					<strong>password:</strong>&#160; <xsl:value-of select="tns:password"/>
			</div>	
			<div class="url">
					<strong>url:</strong>&#160; <xsl:value-of select="tns:url"/>
			</div>	
		</div>
	</xsl:template>	

	<xsl:template match="tns:configuration/tns:server">


	</xsl:template>	

	</xsl:template>	
</xsl:stylesheet>
